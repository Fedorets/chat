let express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    io = require('socket.io')(server),
    port = process.env.PORT || 3000;

server.listen(port, function () {
    console.log('Server listening at port %d', port);
});

/**
 * Routing
 */
app.use(express.static(__dirname + '/public'));

/**
 * Chatroom
 */

let numUsers = 0;

io.on('connection', function (socket) {
    let addedUser = false;

    socket.on('new message', function (data) {

        socket.broadcast.emit('new message', {
            username: socket.username,
            message: data
        });
    });

    socket.on('add user', function (username) {
        if (addedUser) return;
        socket.username = username;
        ++numUsers;
        addedUser = true;
        socket.emit('login', {
            numUsers: numUsers
        });

        /**
         * echo globally (all clients) that a person has connected
         */
        socket.broadcast.emit('user joined', {
            username: socket.username,
            numUsers: numUsers
        });
    });

    socket.on('disconnect', function () {
        if (addedUser) {
            --numUsers;

            /**
             * echo globally that this client has left
             */
            socket.broadcast.emit('user left', {
                username: socket.username,
                numUsers: numUsers
            });
        }
    });

    socket.on('image b64', function (data) {
        console.log('Data: ', data);
        socket.broadcast.emit('image b64', {
           username: socket.username,
           file: data.file
        });
    })
});
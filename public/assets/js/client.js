
$(function() {
    // Initialize variables
    let usernameInput = $('.login__input'),
        messages = $('.chat__field'),
        inputMessage = $('.chat__input'),
        chatForm = $('.chat__form'),
        loginForm = $('.login-form'),
        loginPage = $('.login'),
        fileInput = $('#file'),
        chatPage = $('.chat');

    let username,
        connected = false,
        socket = io();

    $('.scrollbar-outer').scrollbar();

    /**
     * Click after set name
     */
    loginForm.submit(($event) => {
        $event.preventDefault();
        setUsername();
    });

    /**
     * call function sendMessage
     */
    chatForm.submit(($event) => {
        $event.preventDefault();
        sendMessage();
    });

    /**
     *  Sets the client's username and show/hide login-chat
     */
    function setUsername () {
        username = cleanInput(usernameInput.val().trim());
        usernameInput.val('');
        if (username) { //if user exist
            loginPage.fadeOut();
            chatPage.show();
            socket.emit('add user', username);
        }
    }

    /**
     * Send a chat message
     */
    function sendMessage () {
        let message = inputMessage.val();
        inputMessage.val('');
        if (message && connected) {
            inputMessage.val('');
            addChatMessage({
                username: username,
                message: message
            });
            socket.emit('new message', message);
        }
    }

    /**
     * Notification message
     * @param message  - text of notification
     */
    function addNotification (message) {
        let el = $('<div>').addClass('chat__notification').text(message);
        addMessageElement(el);
    }

    /**
     * Add message to chat
     * @param data - obj with username and text or file
     */
    function addChatMessage (data) {
        console.log(data);
        let usernameDiv = $('<div class="chat__name"/>')
            .text(data.username);
        let messageBody, authorClass;
        if(data.message) {
            messageBody = $('<div class="chat__field-text">')
            .text(data.message); }
        else if(data.file) {
            messageBody = $('<img class="chat__img" src="'+ data.file +'"/>')
        }
        else { console.log('No way!'); }
        data.username === username ? authorClass = 'own' : '';
        let messageDiv = $('<div class="chat__message"/>')
            .data('username', data.username)
            .addClass(authorClass)
            .append(usernameDiv, messageBody);
        console.log(messageDiv);
        addMessageElement(messageDiv);
    }

    /**
     * append message to chat
     * @param el - div with fields
     */
    function addMessageElement (el) {
        messages.append(el);
        messages[0].scrollTop = messages[0].scrollHeight;
    }

    /**
     * clear input field
     * @param input - html element
     */
    function cleanInput (input) {
        return $('<div/>').text(input).text();
    }

    /**
     * For notification message
     * @param data - number user
     */
    function addCountUserMessage (data) {
        let message = '';
        if (data.numUsers === 1) {
            message += "there's 1 user";
        } else if(data.numUsers === 0) {
            message += "there no users";
        }
        else {
            message += "there are " + data.numUsers + " users";
        }
        addNotification(message);
    }

    /**
     * Watcher file input
     */
    fileInput.change(($event) => {
        let files = $event.target.files;
        uploadImage(files);
    });

    function uploadImage(files) {
        let reader = new FileReader();
        let message ={};
        console.log(files);
        reader.onload = function(e){
            message.username = username;
            message.file = e.target.result;
            socket.emit('image b64', message);
            addChatMessage(message);
        };
        reader.readAsDataURL(files[0]);
    }

    /**
     * Socket events
     * Whenever the server emits some 'event', call corresponding function
     */
    socket.on('login', function (data) {
        connected = true;
        addCountUserMessage(data);
    });

    socket.on('image b64', function (data) {
       addChatMessage(data);
    });

    socket.on('user left', function (data) {
        addNotification(data.username + ' left');
        console.log('LEFT');
        addCountUserMessage(data);
    });

    socket.on('new message', function (data) {
        addChatMessage(data);
    });

    socket.on('user joined', function (data) {
        addNotification(data.username + ' joined');
        addCountUserMessage(data);
    });

    socket.on('disconnect', function () {
        addNotification('you have been disconnected');
    });

});